#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Plugin;

unit class Matrix::Bot::Plugin::AutoAcceptInvites is Matrix::Bot::Plugin;

multi method handle-invite ($e --> Bool) {
	self.log.info("Joining room {$e.room}, invited by {$e.user}");
	True;
}

=begin pod

=NAME    Matrix::Bot::Plugin::AutoAcceptInvites
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
